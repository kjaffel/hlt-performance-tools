import awkward as ak

class TagAndProbe(object):

    @staticmethod
    def close_muon_tag(jets, muons, muon_dr=0.4, jet_dr=1.4, btag=0.3, nminjets=2, jet_pt=40., muon_pt=5.):
        """_summary_

        Args:
            jets (PtEtaPhiMCandidate): Jet collection
            muons (PtEtaPhiMCandidate): Muon collections 
            muon_dr (float, optional): Maxiumum distance between muon to tag jet. Defaults to 0.4.
            jet_dr (float, optional): Minimum distance between tag to probe jet. Defaults to 1.4.
            btag (float, optional):  btag value for tag-jet. Defaults to 0.3.
            nminjets (int, optional):  Minimum number of jets per event. Defaults to 2.
            jet_pt (float, optional): Minimum pt of jets. Defaults to 40.
            muon_pt (float, optional): Minimum pt of muon. Defaults to 5.

        Returns:
            PtEtaPhiMCandidate Pairs: Pairs of jet with fields "tag" and "probe". The probe jets
                                      have to be probed by the user afterwards 
                                      The tag-jet needs to have a muon close by
        """        
        jets["muon"] = jets.nearest(muons, threshold=muon_dr)
        # only work with jets that have a close muon
        jets = jets[~ak.is_none(jets.muon)]
        # kinnematic and object cuts
        tnp_jet_mask =  (ak.num(jets.pt) >= nminjets) & (ak.num(jets.pt > jet_pt ) >= 2) & (jets.pt > 40.)
        tnp_muon_mask = (ak.num(muons.pt) >= 1.) & (ak.any(muons.pt > 5., axis=-1))
        jets = jets[tnp_jet_mask & tnp_muon_mask]

        tnp_jet_pairs = ak.combinations(jets, 2, fields=["tag", "probe"])
        tnp_jet_pairs = tnp_jet_pairs[tnp_jet_pairs.tag.delta_r(tnp_jet_pairs.probe) > jet_dr]
        tnp_muon_mask = tnp_jet_pairs.probe.delta_r(tnp_jet_pairs.tag.muon) < muon_dr 
        tnp_btag_mask = tnp_jet_pairs.tag.btag > btag 
        tnp_jet_pairs = tnp_jet_pairs[tnp_muon_mask & tnp_btag_mask]
        tnp_jet_pairs = ak.flatten(tnp_jet_pairs)

        pnt_jet_pairs = ak.combinations(jets, 2, fields=["probe", "tag"])
        pnt_jet_pairs = pnt_jet_pairs[pnt_jet_pairs.tag.delta_r(pnt_jet_pairs.probe) > jet_dr]
        pnt_muon_mask = pnt_jet_pairs.probe.delta_r(pnt_jet_pairs.tag.muon) < muon_dr 
        pnt_btag_mask = pnt_jet_pairs.tag.btag > btag
        pnt_jet_pairs = pnt_jet_pairs[pnt_muon_mask & pnt_btag_mask]
        pnt_jet_pairs = ak.flatten(pnt_jet_pairs)

        all_jets = ak.concatenate((tnp_jet_pairs, pnt_jet_pairs), axis=0)
        all_jets = all_jets[~ak.is_none(all_jets)]

        return all_jets

    @staticmethod
    def close_muon_probe(jets, muons, muon_dr=0.4, jet_dr=1.4, btag=0.3, nminjets=2, jet_pt=40., muon_pt=5.):
        """_summary_

        Args:
            jets (PtEtaPhiMCandidate): Jet collection
            muons (PtEtaPhiMCandidate): Muon collections 
            muon_dr (float, optional): Maxiumum distance between muon to probe jet. Defaults to 0.4.
            jet_dr (float, optional): Minimum distance between tag to probe jet. Defaults to 1.4.
            btag (float, optional):  btag value for tag-jet. Defaults to 0.3.
            nminjets (int, optional):  Minimum number of jets per event. Defaults to 2.
            jet_pt (float, optional): Minimum pt of jets. Defaults to 40.
            muon_pt (float, optional): Minimum pt of muon. Defaults to 5.

        Returns:
            PtEtaPhiMCandidate Pairs: Pairs of jet with fields "tag" and "probe". The probe jets
                                      have to be probed by the user afterwards.
                                      The probe-jet needs to have a muon close by
        """        
        jets["muon"] = jets.nearest(muons, threshold=muon_dr)
        # only work with jets that have a close muon
        jets = jets[~ak.is_none(jets.muon)]
        # kinnematic and object cuts
        tnp_jet_mask =  (ak.num(jets.pt) >= nminjets) & (ak.num(jets.pt > jet_pt ) >= 2) & (jets.pt > 40.)
        tnp_muon_mask = (ak.num(muons.pt) >= 1.) & (ak.any(muons.pt > 5., axis=-1))
        jets = jets[tnp_jet_mask & tnp_muon_mask]

        tnp_jet_pairs = ak.combinations(jets, 2, fields=["tag", "probe"])
        tnp_jet_pairs = tnp_jet_pairs[tnp_jet_pairs.tag.delta_r(tnp_jet_pairs.probe) > jet_dr]
        tnp_muon_mask = tnp_jet_pairs.probe.delta_r(tnp_jet_pairs.probe.muon) <muon_dr 
        tnp_btag_mask = tnp_jet_pairs.tag.btag > btag 
        tnp_jet_pairs = tnp_jet_pairs[tnp_muon_mask & tnp_btag_mask]
        tnp_jet_pairs = ak.flatten(tnp_jet_pairs)

        pnt_jet_pairs = ak.combinations(jets, 2, fields=["probe", "tag"])
        pnt_jet_pairs = pnt_jet_pairs[pnt_jet_pairs.tag.delta_r(pnt_jet_pairs.probe) > jet_dr]
        pnt_muon_mask = pnt_jet_pairs.probe.delta_r(pnt_jet_pairs.probe.muon) < muon_dr 
        pnt_btag_mask = pnt_jet_pairs.tag.btag > btag
        pnt_jet_pairs = pnt_jet_pairs[pnt_muon_mask & pnt_btag_mask]
        pnt_jet_pairs = ak.flatten(pnt_jet_pairs)

        all_jets = ak.concatenate((tnp_jet_pairs, pnt_jet_pairs), axis=0)
        all_jets = all_jets[~ak.is_none(all_jets)]

        return all_jets