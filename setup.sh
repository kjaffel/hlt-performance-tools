#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export HLT_PERFORMANCE_TOOLS_PATH=$SCRIPT_DIR
export PYTHONPATH=$PYTHONPATH:$SCRIPT_DIR