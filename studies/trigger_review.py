import argparse
import glob
import json
import os
import awkward as ak
import numpy as np
from coffea import processor
from coffea.nanoevents import NanoAODSchema
from utils.coffea import TriggerReviewProcessor
from plotting.efficiencies import plot_efficiency, plot_efficiencies
from utils.statistics import getError


if __name__ == "__main__":
    """
    Read in args
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true", help="Activate debug settings")
    parser.add_argument("--out", default="plots/out/", help="plot output name")
    parser.add_argument("--fileset", default=None, help="Json to the fileset that you are using")
    args = parser.parse_args()
    base_dir = args.out

    if args.fileset is None:
        raise ValueError("You need to specify a fileset!")
    with open(args.fileset, "r") as fileset_json:
        print(f"Reading in fileset: {args.fileset}")
        fileset = json.load(fileset_json)
    with open("sequences.json", "r") as sequence_json:
        sequences = json.load(sequence_json)["sequences"]

    print(f"Making output directory {args.out}")
    os.makedirs(base_dir, exist_ok=True)
    print("Reading in sequences.json")

    kwargs = {}
    if args.debug:
        kwargs.update({"maxchunks": 1})
        for key in fileset.keys():
            fileset[key] = fileset[key][:1]

    iterative_run = processor.Runner(
        executor=processor.FuturesExecutor(compression=None, workers=32),
        schema=NanoAODSchema,
        **kwargs,
    )
    paths = [
        "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5",
        "PFHT330PT30_QuadPFJet_75_60_45_40",
        "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71",
        "IsoMu24",
        "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5",
        "PFHT400_FivePFJet_100_100_60_30_30",
        "PFHT450_SixPFJet36",
        "PFHT450_SixPFJet36_PFBTagDeepJet_1p59",
        "QuadPFJet70_50_40_30",
        "QuadPFJet70_50_40_30_PFBTagParticleNet_2BTagSum0p65",
        "HLT_DoublePFJets100_PFBTagDeepCSV_p71_v3",
        "HLT_DoublePFJets100_PFBTagDeepJet_p71_v3",
        "HLT_DoublePFJets116MaxDeta1p6_DoublePFBTagDeepCSV_p71_v3",
        "HLT_DoublePFJets116MaxDeta1p6_DoublePFBTagDeepJet_p71_v3",
        "HLT_DoublePFJets128MaxDeta1p6_DoublePFBTagDeepCSV_p71_v3",
        "HLT_DoublePFJets128MaxDeta1p6_DoublePFBTagDeepJet_p71_v3",
        "HLT_DoublePFJets200_PFBTagDeepCSV_p71_v3",
        "HLT_DoublePFJets200_PFBTagDeepJet_p71_v3",
        "HLT_DoublePFJets350_PFBTagDeepCSV_p71_v3",
        "HLT_DoublePFJets350_PFBTagDeepJet_p71_v4",
        "HLT_DoublePFJets40_PFBTagDeepCSV_p71_v3",
        "HLT_DoublePFJets40_PFBTagDeepJet_p71_v3",
        "HLT_Mu12_DoublePFJets100_PFBTagDeepCSV_p71_v3",
        "HLT_Mu12_DoublePFJets100_PFBTagDeepJet_p71_v3",
        "HLT_Mu12_DoublePFJets200_PFBTagDeepCSV_p71_v3",
        "HLT_Mu12_DoublePFJets200_PFBTagDeepJet_p71_v3",
        "HLT_Mu12_DoublePFJets350_PFBTagDeepCSV_p71_v3",
        "HLT_Mu12_DoublePFJets350_PFBTagDeepJet_p71_v3",
        "HLT_Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepCSV_p71_v3",
        "HLT_Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71_v3",
        "HLT_Mu12_DoublePFJets40_PFBTagDeepCSV_p71_v3",
        "HLT_Mu12_DoublePFJets40_PFBTagDeepJet_p71_v3",
        "HLT_Mu12_DoublePFJets54MaxDeta1p6_DoublePFBTagDeepCSV_p71_v3",
        "HLT_Mu12_DoublePFJets54MaxDeta1p6_DoublePFBTagDeepJet_p71_v3",
        "HLT_PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepCSV_4p5_v5",
        "HLT_PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5_v3",
        "HLT_PFHT330PT30_QuadPFJet_75_60_45_40_v11",
        "HLT_PFHT350_v21",
        "HLT_PFHT370_v19",
        "HLT_PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepCSV_4p5_v10",
        "HLT_PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5_v3",
        "HLT_PFHT400_FivePFJet_100_100_60_30_30_v10",
        "HLT_PFHT400_FivePFJet_120_120_60_30_30_DoublePFBTagDeepCSV_4p5_v10",
        "HLT_PFHT400_FivePFJet_120_120_60_30_30_DoublePFBTagDeepJet_4p5_v3",
        "HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94_v10",
        "HLT_PFHT400_SixPFJet32_DoublePFBTagDeepJet_2p94_v3",
    ]

    runs = {"Run2022{}".format(name): {"lumi": 12.3} for name in ["C", "D", "E", "F", "G"]}

    merge_datasets = {
        "Run3": ["Run2022B", "Run2022C", "Run2022D", "Run2022E", "Run2022F", "Run2022G"],
        "pre-pixel": ["Run2022E"],
        "post-pixel": ["Run2022E"],
        "pre-HCAL": ["Run2022B", "Run2022C", "Run2022D", "Run2022E"],
        "post-HCAL": ["Run2022F", "Run2022G"],
    }

    out = iterative_run(
        fileset,
        treename="Events",
        processor_instance=TriggerReviewProcessor(
            sequences=sequences,
            paths=paths,
            PU_file="configs/pileup_2022_310123.json",
            merge_datasets=merge_datasets,
        ),
    )

    matching_sequences_paths = {
        "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5": "hltBTagPFDeepJet4p5Triple",
        "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71": "hltBTagPFDeepJet0p71Double8Jets30",
        "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5": "hltBTagPFDeepJet4p5Double",
        "PFHT450_SixPFJet36_PFBTagDeepJet_1p59": "hltBTagPFDeepJet1p59Single",
    }

    matching_references_paths = {
        "PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5": [
            "PFHT330PT30_QuadPFJet_75_60_45_40",
            3,
        ],
        "Mu12_DoublePFJets40MaxDeta1p6_DoublePFBTagDeepJet_p71": ["IsoMu24", 2],
        "PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5": [
            "PFHT400_FivePFJet_100_100_60_30_30",
            2,
        ],
        "PFHT450_SixPFJet36_PFBTagDeepJet_1p59": ["PFHT450_SixPFJet36", 1],
    }

    N_bins = 12

    results_dict = {key: {} for key in merge_datasets.keys()}
    for merge_dataset in merge_datasets.keys():
        out_dir = os.path.join(args.out, merge_dataset)
        os.makedirs(out_dir, exist_ok=True)

        for path, values in matching_references_paths.items():
            results_dict[merge_dataset][path] = {}
            reference = values[0]
            index = values[1]

            mask_path = out[merge_dataset][path].value
            mask_reference = out[merge_dataset][reference].value

            try:
                seq = matching_sequences_paths[path]
            except KeyError:
                continue
            print(f"Path:\t{path}")
            print(f"Reference:\t{reference}")
            print(f"Seq:\t{seq}")

            # b_tag_values = np.array(out["btags"].value)
            npvs = np.array(out[merge_dataset]["npvs"].value)
            rec_lumi = np.array(out[merge_dataset]["recorded_lumi"].value)
            n_passing = mask_path & mask_reference
            n_total = mask_reference

            b_tag_values = np.array(out[merge_dataset]["btags"].value)[..., -index]

            # b_tag values are 0 when there is no sequence present
            sanity_mask = (
                (b_tag_values >= 0.0)
                & np.invert(np.isinf(b_tag_values))
                & np.invert(np.isnan(b_tag_values))
            )

            b_tag_values = b_tag_values[sanity_mask]
            n_passing = n_passing[sanity_mask]
            n_total = n_total[sanity_mask]
            npvs = npvs[sanity_mask]
            rec_lumi = rec_lumi[sanity_mask]
            avg_inst_lumi = rec_lumi / 23.31

            """
            plot in b-tag bins
            """
            bins = np.linspace(0, 1.0, num=N_bins - 1, endpoint=False)
            bin_digits = np.digitize(b_tag_values, bins)

            numerator = np.array(
                [np.sum(n_passing[np.where(bin_digits == i)]) for i in range(N_bins)]
            )
            denominator = np.array(
                [np.sum(n_total[np.where(bin_digits == i)]) for i in range(N_bins)]
            )

            efficiency = numerator / denominator
            lower_error, upper_error = getError(numerator, denominator)

            bin_centers = np.diff(np.concatenate((bins, [1.0]))) / 2 + bins
            plot_title = path[: len(path) // 2] + "\n" + path[len(path) // 2 :]
            ymax = np.min((1.0, np.max(efficiency[np.isfinite(efficiency)]) * 1.2))
            plot_efficiency(
                efficiency[1:],
                lower_error[1:],
                upper_error[1:],
                bin_centers,
                title=plot_title,
                path=os.path.join(out_dir, f"{merge_dataset}_eff_b_tag_{seq}.png"),
                xlabel="offline {} b-tag value".format(index),
                ymax=ymax,
            )
            plot_efficiency(
                efficiency[1:],
                lower_error[1:],
                upper_error[1:],
                bin_centers,
                title=plot_title,
                path=os.path.join(out_dir, f"{merge_dataset}_eff_b_tag_{seq}_log.png"),
                xlabel="offline {} b-tag value".format(index),
                log=True,
                ymax=ymax,
            )
            results_dict[merge_dataset][path]["b-tag"] = {
                "efficiency": efficiency[1:],
                "lower_error": lower_error[1:],
                "upper_error": upper_error[1:],
                "bin_centers": bin_centers,
            }
            """
            plot in npvs 
            """
            bins = np.arange(15, 65, 3)
            bin_digits = np.digitize(npvs, bins)

            numerator = np.array(
                [np.sum(n_passing[np.where(bin_digits == i)]) for i in range(len(bins))]
            )
            denominator = np.array(
                [np.sum(n_total[np.where(bin_digits == i)]) for i in range(len(bins))]
            )

            efficiency = numerator / denominator
            lower_error, upper_error = getError(numerator, denominator)

            ymax = np.min((1.0, np.max(efficiency[np.isfinite(efficiency)]) * 1.2))
            # ymax = np.max(efficiency[1:]) * 1.2
            # if np.isnan(ymax) or np.isinf(ymax):
            #     ymax = 1.

            bin_centers = (
                np.diff(np.concatenate((bins, [bins[-1] + (bins[-1] - bins[-2])]))) / 2 + bins
            )
            plot_efficiency(
                efficiency,
                lower_error,
                upper_error,
                bin_centers,
                title=plot_title,
                path=os.path.join(out_dir, f"{merge_dataset}_eff_npv_{path}.png"),
                xlabel="NPV",
                ymax=ymax,
            )
            plot_efficiency(
                efficiency,
                lower_error,
                upper_error,
                bin_centers,
                title=plot_title,
                path=os.path.join(out_dir, f"{merge_dataset}_eff_npv_{path}_log.png"),
                xlabel="NPV",
                log=True,
                ymax=ymax,
            )
            results_dict[merge_dataset][path]["npv"] = {
                "efficiency": efficiency,
                "lower_error": lower_error,
                "upper_error": upper_error,
                "bin_centers": bin_centers,
            }
            """
            plot in inst. lumi 
            """
            bins = np.linspace(6000, 20000, 16)
            bins = np.concatenate((np.array([-1.0]), bins))
            bin_digits = np.digitize(avg_inst_lumi, bins)

            numerator = np.array(
                [np.sum(n_passing[np.where(bin_digits == i)]) for i in range(len(bins))]
            )
            denominator = np.array(
                [np.sum(n_total[np.where(bin_digits == i)]) for i in range(len(bins))]
            )

            efficiency = numerator / denominator
            lower_error, upper_error = getError(numerator, denominator)

            # ymax = np.max(efficiency[np.isfinite(efficiency)]) * 1.2

            plot_title = path[: len(path) // 2] + "\n" + path[len(path) // 2 :]
            bin_centers = (
                np.diff(np.concatenate((bins, [bins[-1] + (bins[-1] - bins[-2])]))) / 2 + bins
            )
            bin_centers[0] = 0.0
            plot_efficiency(
                efficiency,
                lower_error,
                upper_error,
                bin_centers,
                title=plot_title,
                path=os.path.join(out_dir, f"{merge_dataset}_eff_inst_lumi_{path}.png"),
                xlabel="avg. inst. Luminosity /$(\mu b s)$",
                xmin=5000,
                ymax=ymax,
            )
            plot_efficiency(
                efficiency,
                lower_error,
                upper_error,
                bin_centers,
                title=plot_title,
                path=os.path.join(out_dir, f"{merge_dataset}_eff_inst_lumi_{path}_log.png"),
                xlabel="avg. inst. Luminosity /$(\mu b s)$",
                log=True,
                xmin=5000,
                ymax=ymax,
            )
            results_dict[merge_dataset][path]["inst-lumi"] = {
                "efficiency": efficiency,
                "lower_error": lower_error,
                "upper_error": upper_error,
                "bin_centers": bin_centers,
            }

    for path, values in matching_references_paths.items():
        index = values[1]

        bin_centers = list(results_dict.values())[0][path]["b-tag"]["bin_centers"]
        plot_title = path[: len(path) // 2] + "\n" + path[len(path) // 2 :]
        all_eff = np.concatenate(
            [
                efficiency[np.isfinite(efficiency)]
                for efficiency in [
                    results_dict[merge_dataset][path]["b-tag"]["efficiency"]
                    for merge_dataset in merge_datasets.keys()
                ]
            ]
        )
        ymax = np.min((1.0, np.max(all_eff * 1.2)))
        plot_efficiencies(
            [
                results_dict[merge_dataset][path]["b-tag"]["efficiency"]
                for merge_dataset in merge_datasets.keys()
            ],
            [
                results_dict[merge_dataset][path]["b-tag"]["lower_error"]
                for merge_dataset in merge_datasets.keys()
            ],
            [
                results_dict[merge_dataset][path]["b-tag"]["upper_error"]
                for merge_dataset in merge_datasets.keys()
            ],
            bin_centers,
            title=plot_title,
            path=os.path.join(base_dir, f"eff_b-tag_{path}.png"),
            xlabel="offline {} b-tag value".format(index),
            ymax=ymax,
            labels=merge_datasets.keys(),
        )

        bin_centers = list(results_dict.values())[0][path]["npv"]["bin_centers"]
        plot_title = path[: len(path) // 2] + "\n" + path[len(path) // 2 :]
        all_eff = np.concatenate(
            [
                efficiency[np.isfinite(efficiency)]
                for efficiency in [
                    results_dict[merge_dataset][path]["npv"]["efficiency"]
                    for merge_dataset in merge_datasets.keys()
                ]
            ]
        )
        ymax = np.min((1.0, np.max(all_eff * 1.2)))
        plot_efficiencies(
            [
                results_dict[merge_dataset][path]["npv"]["efficiency"]
                for merge_dataset in merge_datasets.keys()
            ],
            [
                results_dict[merge_dataset][path]["npv"]["lower_error"]
                for merge_dataset in merge_datasets.keys()
            ],
            [
                results_dict[merge_dataset][path]["npv"]["upper_error"]
                for merge_dataset in merge_datasets.keys()
            ],
            bin_centers,
            title=plot_title,
            path=os.path.join(base_dir, f"eff_npv_{path}.png"),
            xlabel="NPV",
            log=False,
            ymax=ymax,
            labels=merge_datasets.keys(),
        )

        bin_centers = list(results_dict.values())[0][path]["inst-lumi"]["bin_centers"]
        plot_title = path[: len(path) // 2] + "\n" + path[len(path) // 2 :]
        all_eff = np.concatenate(
            [
                efficiency[np.isfinite(efficiency)]
                for efficiency in [
                    results_dict[merge_dataset][path]["inst-lumi"]["efficiency"]
                    for merge_dataset in merge_datasets.keys()
                ]
            ]
        )
        ymax = np.min((1.0, np.max(all_eff * 1.2)))
        plot_efficiencies(
            [
                results_dict[merge_dataset][path]["inst-lumi"]["efficiency"]
                for merge_dataset in merge_datasets.keys()
            ],
            [
                results_dict[merge_dataset][path]["inst-lumi"]["lower_error"]
                for merge_dataset in merge_datasets.keys()
            ],
            [
                results_dict[merge_dataset][path]["inst-lumi"]["upper_error"]
                for merge_dataset in merge_datasets.keys()
            ],
            bin_centers,
            title=plot_title,
            path=os.path.join(base_dir, f"eff_inst-lumi_{path}.png"),
            xlabel="avg. inst. Luminosity /$(\mu b s)$",
            log=False,
            ymax=ymax,
            xmin=5000,
            labels=merge_datasets.keys(),
        )
