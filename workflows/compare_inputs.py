import argparse
import os
import json
import numpy as np
import tqdm

from coffea import processor
from coffea.nanoevents import BaseSchema

from utils.misc import setup_fileset
from utils.plotting.hists import plot_hist, plot_hist_split_flavour_ratio
from utils.processors import InputProcessor


if __name__ == "__main__":
    print("Be caerful! Numpy warnings are turned off!")
    np.seterr(all="ignore")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "files",
        type=str,
        nargs="+",
        help="List of files to process. Either give root files or a .txt with paths to root files.",
    )
    parser.add_argument(
        "--label",
        type=str,
        nargs="+",
        help="Label for the datasets. Should have the same number of arguments as files",
    )
    parser.add_argument(
        "--skip_keys",
        type=str,
        help="Keys to skip",
    )
    parser.add_argument("--isdata", action="store_true", help="If first dataset is data")
    parser.add_argument("--debug", action="store_true", help="Activate debug settings")
    parser.add_argument(
        "--out",
        default=f"{os.getenv('HLT_PERFORMANCE_TOOLS_PATH')}/plots",
        help="Output directory for plots",
    )
    args = parser.parse_args()

    output_path = args.out
    os.makedirs(output_path, exist_ok=True)
    """
    parse input files or filelist 
    """
    if args.debug:
        maxFiles = 100
    else:
        maxFiles = None

    if args.label is None:
        labels = [f"Dataet-{i}" for i in len(args.files)]
    else:
        labels = args.label

    fileset = setup_fileset(args.files, labels, maxFiles)

    dataset_config = {
        labels[0]: {"is_data": args.isdata},
    }

    with open("utils/plotting/config.json", "r") as openfile:
        plot_config = json.load(openfile)

    if args.debug:
        maxchunks = 500
    else:
        maxchunks = None
    iterative_run = processor.Runner(
        executor=processor.FuturesExecutor(compression=None, workers=32),
        schema=BaseSchema,
        maxchunks=maxchunks,
    )

    out = iterative_run(
        fileset,
        treename="DeepJetNTupler/DeepJetvars",
        processor_instance=InputProcessor(
            fileset.keys(), dataset_config=dataset_config, skip_keys=args.skip_keys
        ),
    )
    for value, name in zip(out["jet_pt"].values(), fileset.keys()):
        print(f"#jets {name}:\t", np.sum(value[1:]))

    pbar = tqdm.tqdm(out.items())
    # pbar.set_description("Plotting input variables")
    for key, value in pbar:
        plot_hist_split_flavour_ratio(
            value,
            os.path.join(output_path, key),
            textstr="Overflow: {}".format("some"),
            xlabel=key,
            ylabel="Counts (normalized)",
            xlim=(
                plot_config.get(key, {}).get("min", None),
                plot_config.get(key, {}).get("max", None),
            ),
            normalize=True,
            log=plot_config.get(key, {}).get("log", False),
        )
