#!/bin/bash



python plot_Nano.py --out plots/NanoAOD/HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ_PFDiJet30.pdf \
    --reference HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ_PFDiJet30 \
    --triggers HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ_PFDiJet30_PFBtagDeepJet_1p5 HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ_PFDiJet30_PFBtagDeepCSV_1p5 \
    --triggerlabels PFBtagDeepJet_1p5 PFBtagDeepCSV_1p5 &

python plot_Nano.py --out plots/NanoAOD/HLT_PFHT330PT30_QuadPFJet_75_60_45_40.pdf \
    --reference HLT_PFHT330PT30_QuadPFJet_75_60_45_40 \
    --triggers HLT_PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepJet_4p5 HLT_PFHT330PT30_QuadPFJet_75_60_45_40_TriplePFBTagDeepCSV_4p5 \
    --triggerlabels TriplePFBTagDeepJet_4p5 TriplePFBTagDeepCSV_4p5 &

python plot_Nano.py --out plots/NanoAOD/HLT_PFHT400_SixPFJet32.pdf \
    --reference HLT_PFHT400_SixPFJet32 \
    --triggers HLT_PFHT400_SixPFJet32_DoublePFBTagDeepJet_2p94 HLT_PFHT400_SixPFJet32_DoublePFBTagDeepCSV_2p94 \
    --triggerlabels DoublePFBTagDeepJet_2p94 DoublePFBTagDeepCSV_2p94 &

python plot_Nano.py --out plots/NanoAOD/HLT_PFHT450_SixPFJet36.pdf \
    --reference HLT_PFHT450_SixPFJet36 \
    --triggers HLT_PFHT450_SixPFJet36_PFBTagDeepJet_1p59 HLT_PFHT450_SixPFJet36_PFBTagDeepCSV_1p59 \
    --triggerlabels PFBTagDeepJet_1p59 PFBTagDeepCSV_1p59 &

python plot_Nano.py --out plots/NanoAOD/HLT_PFHT400_FivePFJet_100_100_60_30_30.pdf \
    --reference HLT_PFHT400_FivePFJet_100_100_60_30_30 \
    --triggers HLT_PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepJet_4p5 HLT_PFHT400_FivePFJet_100_100_60_30_30_DoublePFBTagDeepCSV_4p5 \
    --triggerlabels DoublePFBTagDeepJet_4p5 DoublePFBTagDeepCSV_4p5 &


python plot_Nano.py --out plots/NanoAOD/HLT_QuadPFJet103_88_75_15.pdf \
    --reference HLT_QuadPFJet103_88_75_15 \
    --triggers HLT_QuadPFJet103_88_75_15_PFBTagDeepJet_1p3_VBF2 HLT_QuadPFJet103_88_75_15_PFBTagDeepCSV_1p3_VBF2 HLT_QuadPFJet103_88_75_15_DoublePFBTagDeepJet_1p3_7p7_VBF1  HLT_QuadPFJet103_88_75_15_DoublePFBTagDeepCSV_1p3_7p7_VBF1 \
    --triggerlabels PFBTagDeepJet_1p3_VBF2 PFBTagDeepCSV_1p3_VBF2 DoublePFBTagDeepJet_1p3_7p7_VBF1 DoublePFBTagDeepCSV_1p3_7p7_VBF1 &

python plot_Nano.py --out plots/NanoAOD/HLT_QuadPFJet105_88_76_15.pdf \
    --reference HLT_QuadPFJet105_88_76_15 \
    --triggers HLT_QuadPFJet105_88_76_15_PFBTagDeepJet_1p3_VBF2 HLT_QuadPFJet105_88_76_15_PFBTagDeepCSV_1p3_VBF2 HLT_QuadPFJet105_88_76_15_DoublePFBTagDeepJet_1p3_7p7_VBF1 HLT_QuadPFJet105_88_76_15_DoublePFBTagDeepCSV_1p3_7p7_VBF1 \
    --triggerlabels PFBTagDeepJet_1p3_VBF2 PFBTagDeepCSV_1p3_VBF2 DoublePFBTagDeepJet_1p3_7p7_VBF1 DoublePFBTagDeepCSV_1p3_7p7_VBF1 &

python plot_Nano.py --out plots/NanoAOD/HLT_QuadPFJet111_90_80_15.pdf \
    --reference HLT_QuadPFJet111_90_80_15 \
    --triggers HLT_QuadPFJet111_90_80_15_PFBTagDeepJet_1p3_VBF2 HLT_QuadPFJet111_90_80_15_PFBTagDeepCSV_1p3_VBF2 HLT_QuadPFJet111_90_80_15_DoublePFBTagDeepJet_1p3_7p7_VBF1 HLT_QuadPFJet111_90_80_15_DoublePFBTagDeepCSV_1p3_7p7_VBF1 \
    --triggerlabels PFBTagDeepJet_1p3_VBF2 PFBTagDeepCSV_1p3_VBF2 DoublePFBTagDeepJet_1p3_7p7_VBF1 DoublePFBTagDeepCSV_1p3_7p7_VBF1 &
